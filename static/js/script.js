///<refrence path = "static/js/angular.min.js"/>


 var myapp = angular.module("myapp",["ngRoute"],function($interpolateProvider)
  {
      $interpolateProvider.startSymbol('[[');
      $interpolateProvider.endSymbol(']]');
  });

 
myapp.controller('mycontroller',['$scope','$http',function($scope,$http)
  {
            
      $http.get("http://127.0.1:5000/loaddata")
            .then(function (response) 
              {

                $scope.contents = response.data;

              });
              

  }]);



myapp.config(function($routeProvider,$locationProvider)
  {
      $locationProvider.hashPrefix();

      $routeProvider.when("/",
          {
              controller:"mycontroller",
              templateUrl:"static/pages/index.html"
          }
  );
    });

